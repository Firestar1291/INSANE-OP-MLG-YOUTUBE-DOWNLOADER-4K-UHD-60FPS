
# 4K Youtube Video Downloader (requires Python)

You can download any video up to 4K with audio with this code on your computer. This code natively saves them into your video folder. 

If you find any bugs or glitches, please contact me. 
## Authors
- [@Firestar1291](https://gitlab.com/Firestar1291)
- [@JirR02](https://gitlab.com/JirR02)


## License

[MIT](https://choosealicense.com/licenses/mit/)


## Installation

- Download newest version (with interface)
```bash
$ curl https://gitlab.com/Firestar1291/INSANE-OP-MLG-YOUTUBE-DOWNLOADER-4K-UHD-60FPS/-/raw/main/code/version_03.1.py?inline=false
```
### or 
- Download newest version (no interface)
```bash
$ curl https://gitlab.com/Firestar1291/INSANE-OP-MLG-YOUTUBE-DOWNLOADER-4K-UHD-60FPS/-/raw/main/code/version_02.2.py?inline=false
```
- Download the required software
```bash
$ pip install pytube moviepy ffmpeg pysimplegui
```
- On Linux, you can add an alias in your .bashrc. To do that, you need python installed: 
```bash
$ sudo apt install python3
```
- Create your alias using this command:
```bash
$ echo "alias YOURALIASNAME="path/to/script"" >> .bashrc
```
- To execute, type YOURALIASNAME in bash