"""
Instructions: 
1. In folder where this file is located, do not store any other files, especially webm files
2. Update setup with correct relative path where the finished video will be stored and select your maximum download resolution
3. Setup an alias to execute this file from console: "alias |command name| = python3 |absolute path to this python file|
4. Paste link, hit enter
5. When execution is finished, all files are cleared except the finished video file in your output folder


Changes: 
- Interface version 01.1. to code version 02.2
- improved interface throughout whole conversion process
"""
from pytube import YouTube
from pytube.streams import Stream
import pytube
from moviepy.editor import VideoFileClip, AudioFileClip
from moviepy.editor import *
import time
import re
import ffmpeg
import subprocess
import PySimpleGUI as sg
import requests
import os
from PIL import Image

#Setup
#Absolute Path to Video folder:
videopath = "~/Videos"
#Preferred maximum download resolution
maxres = "2160p"
#Possible values for maxres: "2160p", "1440p", "1080p", "720p", "480p", "360p", "240p", "144p"

#Precode: Deleting any remaining mp4 and webm files files:
subprocess.run("rm *.mp4", shell=True)
subprocess.run("rm *.webm", shell=True)

#functions
reslist = ("2160p", "1440p", "1080p", "720p", "480p", "360p", "240p", "144p")

def sortaudio(eeee):
    eeee = re.sub('[^0-9]', '', eeee)
    eeee = int(eeee)
    return(eeee)

if maxres == "2160p":
    rescounter = 0
if maxres == "1440p":
    rescounter = 1
if maxres == "1080p":
    rescounter = 2
if maxres == "720p":
    rescounter = 3
if maxres == "480p":
    rescounter = 4
if maxres == "360p":
    rescounter = 5
if maxres == "240p":
    rescounter = 6
if maxres == "144p":
    rescounter = 7
if maxres not in reslist:
    print("ERROR: maxres invalid\n Program stopped")
    sys.exit()

retrycounter = 0

#Program
audiolist = []
progprog = "audio"

#Window setup
titlespace = [  #Main Window
    [sg.VStretch()],
    [sg.Push(), sg.Text("Youtube Video Downloader by Firestar1291", font=("Helvetica", 30), text_color="blue"), sg.Push()],
    [sg.VPush()],
    [sg.Push(), sg.Text("Paste your Youtube Link in the field below"), sg.Push()],
    [sg.Push(), sg.Text("", text_color="red", key="-ERRORM-"), sg.Push()],
    [sg.Push(), sg.Input("", justification="c", key="-PASTE-"), sg.Push()],
    [sg.Text("")],
    [sg.Push(), sg.Submit("Submit"), sg.Cancel("Cancel"), sg.Button("Clear", key="-CLEAR-"), sg.Push()],
    [sg.Text("")],
    [sg.Push(), sg.Text("No issues detected", text_color="lime", key="-ERRORC-"), sg.Push()],
    [sg.VPush()],
    [sg.VPush()],
]

error = [   #Error popup window
    [sg.Text("Please insert a  valid youtube video link, e.g. ", justification="c")],
    [sg.Text("https://www.youtube.com/watch?v=dQw4w9WgXcQ", text_color="blue", justification="c")],
    [sg.Cancel("OK")],
]



####### Window interface opens #######
window = sg.Window("Youtube Video Downloader", titlespace, resizable=True, auto_size_text=True)

while True:
    event, values = window.read()
    if event == "Cancel" or event == sg.WIN_CLOSED:
        break
    if event == "Submit":
        link = values["-PASTE-"]
        if "https://www.youtube.com/watch?" in link:
            print(link)
            break
        elif "youtube.com/watch?" not in link and retrycounter == 0:
            window["-ERRORM-"].update("Please insert a  valid youtube video link, e.g. https://www.youtube.com/watch?v=dQw4w9WgXcQ")
            errorwin = sg.Window("Error: Invalid link", error, keep_on_top=True, element_justification="c")
            retrycounter += 1
            event, values = errorwin.read()    
            errorwin.close()
        if "youtube.com/watch?" not in link:
            window["-ERRORC-"].update("Error detected. Try again", text_color = "red")
    if event == "-CLEAR-":
        window["-PASTE-"].update("")


window.close()
####### Window interface closes #######

yt = YouTube(link)

title = re.sub('[^A-Za-z0-9 !]+', '', yt.title)
print("Downloading youtube video with the title: " + title)

# URL of the image you want to download
image_url1 = yt.thumbnail_url
# Send a GET request to the image URL
response = requests.get(image_url1)
# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Get the content of the response (image data)
    image_data = response.content
    # Get the file name from the URL
    image_url = image_url1.split("?")[0]
    file_name = os.path.basename(image_url)
    # Save the image data to the current directory
    with open(file_name, "wb") as file:
        file.write(image_data)
    
    print("Image downloaded successfully.")
else:
    print("Failed to download the image.")

#Convert the image to png and adjust size
convimage1 = Image.open(file_name)
new_height = 360
width, height = convimage1.size
aspect_ratio = width / height
new_width = int(new_height * aspect_ratio)
convimage = convimage1.resize((new_width, new_height))
convimage.save("thumbnail.png", "PNG")
#Delete old version
subprocess.run("rm " + file_name, shell=True)

#Download Menu Window ########################################################################
#Window Setup
downloadprogress = [  #Main Window
    [sg.VStretch()],
    [sg.Push(), sg.Text("Youtube Video Downloader by Firestar1291", font=("Helvetica", 30), text_color="blue"), sg.Push()],
    [sg.VPush()],
    [sg.Push(), sg.Text("Currently converting youtube video with title: " + title), sg.Push()],
    [sg.Push(), sg.Image("thumbnail.png"), sg.Push()],
    [sg.VPush()],
    [sg.Push(), sg.Text("Currently downloading: Step 1/3", key="-PROG-"), sg.Push()],
    [sg.Push(), sg.Text("Downloading audio", key="-CURR-"), sg.Push()],
    [sg.Push(), sg.Button("Start download", key="-DOWNLOAD-"), sg.VPush()],
    [sg.VPush()],
    [sg.Push(), sg.Text("No issues detected", text_color="lime", key="-ERRORC-"), sg.Push()],
]

downloadwindow = sg.Window("Youtube Video Downloader", downloadprogress, resizable=True, auto_size_text=True)

while True:
    event, values = downloadwindow.read()
    if event == "Cancel" or event == sg.WIN_CLOSED:
        subprocess.run("rm thumbnail.png", shell=True)
        break
    if event == "-DOWNLOAD-":
        # Download Audio
        streams = yt.streams
        for stream in streams:
            if stream.type == "audio" and stream.mime_type == "audio/webm":
                audiolist.append(stream.abr)

        audiolist.sort(key=sortaudio, reverse=True)

        for stream in streams:
            if stream.type == "audio" and stream.mime_type == "audio/webm" and stream.abr == audiolist[0]:
                audiotag = stream.itag
                print("Audio version for download:", stream)
                break

        youTube = pytube.YouTube(link)
        stream = youTube.streams.get_by_itag(audiotag)
        stream.download()
        time.sleep(1)

        vidtitle = re.sub(' ', '_', title)
        fulltitle = vidtitle + ".webm"
        subprocess.run("mv *.webm " + fulltitle , shell=True)
        os.rename(fulltitle, "audio.webm")

        #rename audio for video conversion
        subprocess.run("mv audio.webm audio.ext", shell=True)

        #Download Video
        downloadwindow["-PROG-"].update("Currently downloading: Step 2/3")
        downloadwindow["-CURR-"].update("Downloading video")
        downloadwindow.refresh()
        whichres = []
        allstreams = youTube.streams.all()
        for stream in allstreams:
            if stream.resolution in reslist:
                if stream.resolution not in whichres:
                    whichres.append(stream.resolution)

        while True:
            if reslist[rescounter] in whichres:
                finalres = reslist[rescounter]
                print("Final resolution of downloading video: " + finalres )
                break
            elif reslist[rescounter] not in whichres:
                rescounter += 1
            if rescounter == 50:
                print("ERROR: Could not find suitable video file")
                subprocess.run("rm audio.webm", shell=True)
                sys.exit()

        for finalstream in allstreams:
            if finalstream.resolution == finalres:
                number = finalstream.itag
                print("Your video version: ", finalstream)
                if finalstream.mime_type == "video/webm":
                    videoformat = "webm"
                elif finalstream.mime_type == "video/mp4":
                    videoformat = "mp4"
                break

        stream = youTube.streams.get_by_itag(number)
        stream.download()
        subprocessvar=("mv *." + videoformat + " " + vidtitle + "." + videoformat)
        subprocess.run(subprocessvar, shell=True)
        subprocess.run("mv audio.ext audio.webm", shell=True)

        downloadwindow["-PROG-"].update("Currently converting: Step 3/3")
        downloadwindow["-CURR-"].update("Converting full video")
        downloadwindow.refresh()

        subprocess.run("ffmpeg -i " + vidtitle + "." + videoformat + " -i audio.webm -c:v copy -c:a aac output.mp4", shell=True)

        time.sleep(1)
        os.remove("audio.webm")
        os.remove(vidtitle + "." + videoformat)

        time.sleep(1)
        os.rename("output.mp4", vidtitle + ".mp4")
        subprocess.run("mv " + vidtitle + ".mp4 " + videopath, shell=True)


        finish = [   #Code finished
            [sg.Text("Conversion succesfull:", justification="c")],
            [sg.Text("Your video has been saved to your video folder", justification="c")],
            [sg.Cancel("OK")],
        ]

        finishwin = sg.Window("Conversion finished", finish, keep_on_top=True, element_justification="c")
        event, values = finishwin.read()    
        finishwin.close()
        downloadwindow.close()
        break

print("Code ended")
subprocess.run("rm thumbnail.png", shell=True)