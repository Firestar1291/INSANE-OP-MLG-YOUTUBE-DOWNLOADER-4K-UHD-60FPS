"""
Instructions: 
1. In folder where this file is located, do not store any other files, especially webm files
2. Update setup with correct relative path where the finished video will be stored and select your maximum download resolution
3. Setup an alias to execute this file from console: "alias |command name| = python3 |absolute path to this python file|
4. Paste link, hit enter, select correct itags
5. When execution is finished, all files are cleared except the finished video file in your output folder


Changes: 
- Fix: Can now download every video, even if format is mp4
- Improved output display
"""

from pytube import YouTube
from pytube.streams import Stream
import pytube
from moviepy.editor import VideoFileClip, AudioFileClip
from moviepy.editor import *
import time
import re
import ffmpeg
import subprocess

#Setup
#Absolute Path to Video folder:
videopath = "~/Videos"
#Preferred maximum download resolution
maxres = "2160p"
#Possible values for maxres: "2160p", "1440p", "1080p", "720p", "480p", "360p", "240p", "144p"

#Precode: Deleting any remaining mp4 and webm files files:
subprocess.run("rm *.mp4", shell=True)
subprocess.run("rm *.webm", shell=True)

#functions
reslist = ("2160p", "1440p", "1080p", "720p", "480p", "360p", "240p", "144p")

def sortaudio(eeee):
    eeee = re.sub('[^0-9]', '', eeee)
    eeee = int(eeee)
    return(eeee)

if maxres == "2160p":
    rescounter = 0
if maxres == "1440p":
    rescounter = 1
if maxres == "1080p":
    rescounter = 2
if maxres == "720p":
    rescounter = 3
if maxres == "480p":
    rescounter = 4
if maxres == "360p":
    rescounter = 5
if maxres == "240p":
    rescounter = 6
if maxres == "144p":
    rescounter = 7
if maxres not in reslist:
    print("ERROR: maxres invalid\n Program stopped")
    sys.exit()

#Program
audiolist = []
link = input("#####################\n#Paste youtube link!#\n#####################\n")
yt = YouTube(link)

title = re.sub('[^A-Za-z0-9 !]+', '', yt.title)
print("Downloading youtube video with the title: " + title)

# Download Audio
streams = yt.streams
for stream in streams:
    if stream.type == "audio" and stream.mime_type == "audio/webm":
        audiolist.append(stream.abr)

audiolist.sort(key=sortaudio, reverse=True)

for stream in streams:
    if stream.type == "audio" and stream.mime_type == "audio/webm" and stream.abr == audiolist[0]:
        audiotag = stream.itag
        print("Audio version for download:", stream)
        break

youTube = pytube.YouTube(link)
stream = youTube.streams.get_by_itag(audiotag)
stream.download()
time.sleep(1)

vidtitle = re.sub(' ', '_', title)
fulltitle = vidtitle + ".webm"
subprocess.run("mv *.webm " + fulltitle , shell=True)
os.rename(fulltitle, "audio.webm")

#rename audio for video conversion
subprocess.run("mv audio.webm audio.ext", shell=True)

#Download Video
whichres = []
allstreams = youTube.streams.all()
for stream in allstreams:
    if stream.resolution in reslist:
        if stream.resolution not in whichres:
            whichres.append(stream.resolution)

while True:
    if reslist[rescounter] in whichres:
        finalres = reslist[rescounter]
        print("Final resolution of downloading video: " + finalres )
        break
    elif reslist[rescounter] not in whichres:
        rescounter += 1
    if rescounter == 50:
        print("ERROR: Could not find suitable video file")
        subprocess.run("rm audio.webm", shell=True)
        sys.exit()

for finalstream in allstreams:
    if finalstream.resolution == finalres:
        number = finalstream.itag
        print("Your video version: ", finalstream)
        if finalstream.mime_type == "video/webm":
            videoformat = "webm"
        elif finalstream.mime_type == "video/mp4":
            videoformat = "mp4"
        break

print("Now downloading: video")
stream = youTube.streams.get_by_itag(number)
stream.download()
subprocessvar=("mv *." + videoformat + " " + vidtitle + "." + videoformat)
subprocess.run(subprocessvar, shell=True)
subprocess.run("mv audio.ext audio.webm", shell=True)

subprocess.run("ffmpeg -i " + vidtitle + "." + videoformat + " -i audio.webm -c:v copy -c:a aac output.mp4", shell=True)

time.sleep(1)
os.remove("audio.webm")
os.remove(vidtitle + "." + videoformat)

time.sleep(1)
os.rename("output.mp4", vidtitle + ".mp4")
subprocess.run("mv " + vidtitle + ".mp4 " + videopath, shell=True)

print("Conversion succesfull: Your video has been saved to your video folder")