"""
Instructions: 
1. In folder where this file is located, do not store any other files, especially webm files
2. Update setup with correct relative path where the finished video will be stored
3. Setup an alias to execute this file from console: "alias |command name| = python3 |absolute path to this python file|
4. Paste link, hit enter, select correct itags
5. When execution is finished, all files are cleared except the finished video file in your output folder


Changes: Fixing problem of rare error occurring with file name
"""

from pytube import YouTube
from pytube.streams import Stream
import pytube
from moviepy.editor import VideoFileClip, AudioFileClip
from moviepy.editor import *
import time
import re
import ffmpeg
import subprocess

#Setup
#Absolute Path to Video folder:
videopath = "~/Videos"



reslist = ("2160p", "1440p", "1080p", "720p", "480p", "360p", "240p", "144p"
)
audiolist = []
#link = "https://www.youtube.com/watch?v=EQOarcurXfY"
link = input("#####################\n#Paste youtube link!#\n#####################\n")
yt = YouTube(link)

title = re.sub('[^A-Za-z0-9 !]+', '', yt.title)
print(title)

# Download Audio
streams = yt.streams
for stream in streams:
    if stream.type == "audio" and stream.mime_type == "audio/webm":
        print(stream)

audiotag = input("#########################\n#Type in itag for audio!#\n#########################")

youTube = pytube.YouTube(link)
stream = youTube.streams.get_by_itag(audiotag)
stream.download()
time.sleep(1)

vidtitle = re.sub(' ', '_', title)
fulltitle = vidtitle + ".webm"
print(title)
subprocess.run("mv *.webm " + fulltitle , shell=True)
os.rename(fulltitle, "audio.webm")

#rename audio for video conversion
subprocess.run("mv audio.webm audio.mp4", shell=True)

#Download Video
allstreams = youTube.streams.all()
for stream in allstreams:
    if stream.mime_type == "video/webm" and stream.resolution in reslist:
        print(stream)

number = input("Which itag? ")
stream = youTube.streams.get_by_itag(number)
stream.download()
subprocess.run("mv *.webm " + fulltitle, shell=True)
#os.rename(title + ".webm", vidtitle + ".webm")
subprocess.run("mv audio.mp4 audio.webm", shell=True)

subprocess.run("ffmpeg -i " + vidtitle + ".webm -i audio.webm -c:v copy -c:a aac output.mp4", shell=True)

time.sleep(1)
os.remove("audio.webm")
os.remove(vidtitle + ".webm")

time.sleep(1)
os.rename("output.mp4", vidtitle + ".mp4")
subprocess.run("mv " + vidtitle + ".mp4 " + videopath, shell=True)